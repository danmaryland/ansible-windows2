﻿/*
 * Created by Ranorex
 * User: Administrator
 * Date: 2/15/2019
 * Time: 1:33 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace cie_Test_Ranorex_8._._
{
    /// <summary>
    /// Description of AssertInnerText.
    /// </summary>
    [TestModule("9DF48A97-FCD9-4BFA-B1DD-C0A0119A346C", ModuleType.UserCode, 1)]
    public class CieHelloWorldAppTest : ITestModule
    {
    	public static cie_Test_Ranorex_8_2_1Repository repo = cie_Test_Ranorex_8_2_1Repository.Instance;
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CieHelloWorldAppTest()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            //Dave comment this method out when running the test in the CIE
            
            //Open Chrome and connect to Chad's Hello World App on AWS.
            Report.Log(ReportLevel.Info, "Website", "Opening web site 'http://ec2-54-166-71-97.compute-1.amazonaws.com/welcome-to-cie/index.html' with browser 'Chrome' in normal mode.", new RecordItemIndex(0));
            Host.Current.OpenBrowser("http://ec2-54-166-71-97.compute-1.amazonaws.com/welcome-to-cie/index.html", "Firefox", "", false, false, false, false, false);
            Delay.Milliseconds(100);
            
            //Dave uncomment the method below to connect to the hello world app in the CIE
            
            //Open Chrome and connect to CIE Hello World App on SIPR.
//            Report.Log(ReportLevel.Info, "Website", "Opening web site 'http://s70su02ce02.dgs-x.cte.dcgs.af.smil.mil/welcome-to-cie/index.html' with browser 'Chrome' in normal mode.", new RecordItemIndex(0));
//            Host.Current.OpenBrowser("http://s70su02ce02.dgs-x.cte.dcgs.af.smil.mil/welcome-to-cie/index.html", "Chrome", "", false, false, false, false, false);
//            Delay.Milliseconds(100);

            //Assert that Version 2 string is showing on the landing page.
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'WelcomeToTheCIEMozillaFirefox.Version21'.", repo.WelcomeToTheCIEMozillaFirefox.Version21Info, new RecordItemIndex(1));
            Validate.Exists(repo.WelcomeToTheCIEMozillaFirefox.Version21Info);
            Delay.Milliseconds(100);
            
            //Assert the "does nothing" button is showing on the landing page.
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnything'.", repo.WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnythingInfo, new RecordItemIndex(2));
            Validate.Exists(repo.WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnythingInfo);
            Delay.Milliseconds(0);
            
            //Assert the string on the button states "This button doesn't do anything".
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Text='This button doesn't do anything') on item 'WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnything'.", repo.WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnythingInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.WelcomeToTheCIEMozillaFirefox.ThisButtonDoesntDoAnythingInfo, "Text", "This button doesn't do anything");
            Delay.Milliseconds(100);   	
            	
        }
    }
}
