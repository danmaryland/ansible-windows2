﻿/*
 * Created by Ranorex
 * User: Administrator
 * Date: 2/27/2019
 * Time: 4:36 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace cie_Test_Ranorex_8._._
{
    /// <summary>
    /// Description of CieAssertFirefoxInstall.
    /// </summary>
    [TestModule("AB3DA4D2-6CB9-4B3B-94C0-4A184297A76A", ModuleType.UserCode, 1)]
    public class CieAssertFirefoxInstall : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CieAssertFirefoxInstall()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
     
            //Open Firefox and nav to google.com.
            Report.Log(ReportLevel.Info, "Website", "Opening web site 'www.google.com' with browser 'Firefox' in normal mode.", new RecordItemIndex(0));
            
            try {
            	
            Host.Current.OpenBrowser("www.google.com", "Firefox", "", false, false, false, false, false);
            
            Report.Log(ReportLevel.Info, "System Out", "Firefox is installed as expected", new RecordItemIndex(1));
            
            Delay.Milliseconds(3000);
            
            Host.Local.KillBrowser("Firefox");
            
            } catch (Exception e) {
            	
            	Report.Log(ReportLevel.Error, "System Out", "Test failed due to exception " + e);
            
            }
                  
        }
    }
}
